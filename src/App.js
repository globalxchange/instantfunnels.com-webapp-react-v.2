
import reactDom from 'react-dom';
import React ,{useEffect,useState}from 'react'
import AOS from "aos";

import Home from './Component/HomePage/Homepage'

function App() {
  
React.useEffect(() => {
  AOS.init({ duration: 2000 });

  return () => {

  }
}, [])
  return (
 <Home/>
  );
}

export default App;
